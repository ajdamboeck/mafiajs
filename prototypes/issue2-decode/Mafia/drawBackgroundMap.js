function drawBackgroundMap(cols, rows) {
  for (let r = 0; r < rows; r++) {
    for (let c = 0; c < cols; c++) {
      // map colors here: https://www.december.com/html/spec/colorsvg.html
      switch (mapImportArray[r][c]) {
        case 1: // park
          col = color('mediumseagreen');
          break;
        case 2: // house
          col = color('darksalmon');
          break;
        case 3: // road
          col = color('white');
          break;
        case 4: // buildings darker grey
          col = color('dimgray');
          break;
        case 5: // buildings grey
          col = color(230);
          break;
        case 6: // water
          col = color('blue');
          break;
        case 7: // port
          col = color('red');
          break;
        case 8: // underground
          col = color('blue');
          break;

        default:
          col = color('black');
          break;
      }

      fill(col);
      strokeWeight(1);
      stroke(51);
      rect(c * 32, r * 32, 32, 32);

      textAlign(CENTER, BASELINE);
      fill(99);
      // text(mapImportArray[r][c], c * 32 + 16, r * 32 + 16);
    }
  }
  mapBackground = mapImportArray;
}
