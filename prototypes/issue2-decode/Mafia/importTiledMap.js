function importTiled(imported1DArray, cols, rows) {
  mapImportArray = createEmpty2dArray(cols, rows);
  let tempTiledImport = [ ...imported1DArray ];
  for (let i = 0; i < rows; i++) {
    for (let j = 0; j < cols; j++) {
      let tiledId = tempTiledImport.splice(0, 1);
      mapImportArray[i][j] = tiledId[0];
    }
  }
  return mapImportArray;
}
