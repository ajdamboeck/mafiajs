function keyReleased() {
  if (key === 'ArrowRight') {
    x += 1;
    checkForValidMove(key);
  }
  if (key === 'ArrowLeft') {
    x -= 1;
    checkForValidMove(key);
  }
  if (key === 'ArrowUp') {
    y -= 1;
    checkForValidMove(key);
  }
  if (key === 'ArrowDown') {
    y += 1;
    checkForValidMove(key);
  }
  console.log(`id für ${x},${y}  : ${mapBackground[y][x]}`);
  return key;
}

function checkForValidMove(key) {
  switch (mapBackground[y][x]) {
    case 3:
      movesLeftInTurn--;
      console.log(`on the Road (again), moves left this turn: ${movesLeftInTurn}`);
      break;
    default:
      console.log('off road / block!');
      switch (key) {
        case 'ArrowRight':
          x -= 1;
          break;
        case 'ArrowLeft':
          x += 1;
          break;
        case 'ArrowUp':
          y += 1;
          break;
        case 'ArrowDown':
          y -= 1;
          break;
      }
  }
}
