'use strict';

// imports

let cols; // breite (40), x
let rows; // höhe (25), y
let tileWidth; // kachelbreite aus tiled-json
let tileHeight; // kachelhöhe aus tiled-json
let jsonDataMap;
let eArray = [];
let mapImportArray = [];
let tileId;
let mapBackground;
let x = 18;
let y = 2;
let fps = 77;
let movesLeftInTurn = 4;

// FUNCTIONS

function preload() {
  jsonDataMap = loadJSON('res/mafia_map.json');
  // jsonDataMap = loadJSON('res/map10x10.json');
  // jsonDataMap = loadJSON('res/map_smol.json');
}

function setup() {
  const imported1DArray = jsonDataMap.layers[0].data;
  const cols = jsonDataMap.layers[0].width;
  const rows = jsonDataMap.layers[0].height;
  const tileWidth = jsonDataMap.tilewidth;
  const tileHeight = jsonDataMap.tileheight;
  // console.log(`setup() called. cols: ${cols}, rows: ${rows} `);

  frameRate(fps);

  ellipseMode(CORNER);

  createCanvas(tileWidth * cols, tileHeight * rows);
  importTiled(imported1DArray, cols, rows);
}
function draw() {
  const cols = jsonDataMap.layers[0].width;
  const rows = jsonDataMap.layers[0].height;
  background(111);
  drawBackgroundMap(cols, rows);

  fill('violet');
  stroke(0);
  ellipse(x * 32, y * 32, 32, 32);
}
