'use strict';

const mafia = new Mafia();
let city = {};
let cityData = {};
let font = {};
let glyphs = {};
let img;

let drawLayer = function(p) {
  p.preload = function() {
    cityData = p.loadJSON(mafia.config.map);
    img = p.loadImage('res/street.jpg');
    font = p.loadFont('res/danielbk.ttf');
    glyphs = p.loadFont('res/C64_Pro_Mono-STYLE.otf');
  }
  p.setup = function() {
    mafia.init(cityData, p);
  }
  p.draw = function() {
    p.image(img, 0, 0);
    p.tint(88, 88, 88);
    mafia.city.draw(p);
  }
};

let layer1 = new p5(drawLayer);

// klassen als interface
