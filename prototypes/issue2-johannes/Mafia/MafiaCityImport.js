class MafiaCityImport {
  constructor({ width, height, tilewidth: cellSize, layers }) {
    this.walls = mafia.createLayer(
      this.reorderCells(layers[0].data, width, height),
      width,
      height
    );
    return new MafiaCity({
      width,
      height,
      cellSize,
      walls: this.walls
    });
  }
  reorderCells(cells, w, h) {
    // Tiles werden aus Tiled von Links nach Unten exportiert.
    // Wir brauchen für unsere Zwecke ein Array welches
    // von Oben nach Rechts sortiert ist.
    let reorderedCells = new Array();
    // Dafür benutzen wir ein leeres Grid
    let mappingGrid = mafia.createGrid(w, h);
    // In welches wir die Werte von Links nach Unten eintragen
    for (let y = 0; y < h; y++) {
      for (let x = 0; x < w; x++) {
        let cellData = cells.splice(0, 1);
        mappingGrid[x][y] = cellData[0];
      }
    }
    // Um es dann von Oben nach Rechts wieder zusammenzusetzen
    for (let x = 0; x < w; x++) {
      for (let y = 0; y < h; y++) {
        reorderedCells.push(mappingGrid[x][y]);
      }
    }
    return reorderedCells;
  }
}
