class MafiaCity {
  constructor({ width, height, cellSize, walls }) {
    this.width = width;
    this.height = height;
    this.cellSize = cellSize; // Number divisible by 2
    this.walls = walls; // Grid of cells
  }
  draw(p5) {
    let cellType = [];
    for (let x = 0; x < this.width; x++) {
      for (let y = 0; y < this.height; y++) {
        if (this.walls[x][y]) {
          cellType = this.walls[x][y].type;
          p5.fill(this.walls[x][y].color);
          p5.stroke(88);
          p5.strokeWeight(.1);
          p5.rect(
            x * this.cellSize + this.cellSize / 2,
            y * this.cellSize + this.cellSize / 2,
            this.cellSize,
            this.cellSize
          );
          p5.fill(66);
          p5.noStroke();
          p5.textFont(font, 8);
          p5.text(
            cellType,
            x * this.cellSize + this.cellSize / 2,
            y * this.cellSize + this.cellSize / 2
          );
        }
      }
    }
  }
}
