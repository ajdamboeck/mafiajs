class Mafia {
  constructor() {
    this.config = new MafiaConfig();
    this.city = {};
  }
  init(tiledMapJSON, p5) {
    this.city = new MafiaCityImport(tiledMapJSON);
    this.setupCanvas(p5);
  }
  setupCanvas(p5, loop = false) {
    p5.createCanvas(
      this.city.width * this.city.cellSize,
      this.city.height * this.city.cellSize
    );
    p5.frameRate(this.config.framerate);
    p5.rectMode(p5.CENTER);
    p5.textAlign(p5.CENTER, p5.CENTER);
    loop ? p5.noLoop() : p5.loop(); // Run once and stop
  }
  createGrid(w, h) {
    let grid = new Array(w);
    for (let i = 0; i < grid.length; i++) {
      grid[i] = new Array(h);
    }
    return grid;
  }
  createLayer(cells, w, h) {
    let layer = mafia.createGrid(w, h);
    for (let x = 0; x < w; x++) {
      for (let y = 0; y < h; y++) {
        let cellType = cells.splice(0, 1);
        cellType = cellType[0];
        if (cellType != 3) {
          let cell = new MafiaCityCell({ x, y, type: cellType });
          layer[x][y] = cell;
        }
      }
    }
    return layer;
  }
}
