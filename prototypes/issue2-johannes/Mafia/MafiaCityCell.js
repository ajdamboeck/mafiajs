class MafiaCityCell {
  constructor({ x, y, type, blocking = true }) {
    this.x = x;
    this.y = y;
    this.type = type;
    this.color = 0;
    this.blocking = blocking;
    this.setColor();
  }
  setColor() {
    switch (this.type) {
      case 1: // park
        this.color = 'rgb(120, 160, 120)' ;
        break;
      case 2: // house
        this.color = 'rgb(99, 99, 99)';
        break;
      case 3: // road
        this.color = 33
        break;
      case 4: // buildings darker grey
        this.color = 'rgb(143, 124, 105)';
        break;
      case 5: // buildings grey
        this.color = 'rgb(180, 180, 180)';
        break;
      case 6: // water
        this.color = 'rgb(90, 100, 160)';
        break;
      case 7: // port
        this.color = 'rgb(255, 0, 0)';
        break;
      default:
        this.color = 'rgb(211, 255, 182)';
        break;
    }
  }
}
