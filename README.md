mafiaJS
=========

MafiaJS is an attempt to port the glorious C64-Hit Mafia(by Igelsoft) into a more modern environment. 

The first Stage will consist mostly of porting the Gameplay to JS including:

  - ~~Recreating the Style of the original~~
  - Switching the Setting(1920 Chicago) to a more modern one  
  - Browserbased 1-4 Player Multiplayer and Hot-Seat Mode
  - (Really! ;) Stunning 2D Visuals

Stage two will be: 

  - Replacing the static map with a procedural generated one
  - Create bigger maps
  - GangsterRandomizer for ReplayValue
  - Theming (Map/Client)



Version
----

0.0.1

Team
-----------

Th mafiaJS-Team :

* [@brainiac05] - awesome knowitall... not!
* [@bloodbasser] - riki-noob
* [@hageroth] - vocalist
* [@decoderr] - mapologist 

Installation
--------------

```sh
git clone https://gitlab.com/ajdamboeck/mafiajs.git

```

##### Instructions in following README.md files

* README.md
* res/CaseStudies/README.md


License
----

NOTYET


**Free Software, Hell Yeah!**

[@brainiac05]:http://twitter.com/brainiac05
[@hageroth]:http://twitter.com/hageroth
[@bloodbasser]:http://twitter.com/bloodbasser
[@decoderr]:https://gitlab.com/decoderr